# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = positioning_example

CONFIG += sailfishapp
QT += positioning

SOURCES += src/positioning_example.cpp

OTHER_FILES += qml/positioning_example.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    qml/pages/SecondPage.qml \
    rpm/positioning_example.changes.in \
    rpm/positioning_example.spec \
    rpm/positioning_example.yaml \
    translations/*.ts \
    positioning_example.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/positioning_example-de.ts
QML_ROOT_PATH = /home/vood/SailfishOS/mersdk/targets/SailfishOS-i486/usr/lib/qt5/qml
